#include "CourierManager.h"
#include "constants.h"
#include <ctime>
#include "ExpectsCourierState.h"
#include "DeliverCourierState.h"

CourierManager::CourierManager()
{
	initialize(PositionType::Random);
}


CourierManager::~CourierManager()
{
	release();
}

void CourierManager::update(double dt)
{
	for (auto &courier : couriers)
	{
		courier->update(couriers, dt);
	}
}

void CourierManager::initialize(PositionType positionType)
{
	release();

	srand(time(nullptr));
	Courier *courier;
	for (int i = 0; i < Constants::CourierMaxCount; ++i)
	{
		courier = new Courier();

		courier->setState(std::make_shared<ExpectsCourierState>(courier));

		if (positionType == PositionType::Random)
		{
			courier->setX((rand() % Constants::WindowWidth) - (Constants::WindowWidth / 2));
			courier->setY((rand() % Constants::WindowHeight) - (Constants::WindowHeight / 2));
		}
		else if (positionType == PositionType::Circle)
		{
			float x = Constants::WindowWidth / 2.0;
			float y = Constants::WindowHeight / 2.0;
			float alpha = (Constants::PI / (Constants::CourierMaxCount / 2.0)) * i;

			courier->setX(0 + cos(alpha) * Constants::Radius);
			courier->setY(0 + sin(alpha) * Constants::Radius);
		}
		
		courier->setSpeed(Constants::CourierSpeedMin + (rand() / static_cast<float>(RAND_MAX)) * (Constants::CourierSpeedMax - Constants::CourierSpeedMin));

		couriers.push_back(courier);
	}

	for (int i = 0; i < Constants::ActiveCourierCount && i < Constants::CourierMaxCount; ++i)
	{
		Courier *randomCourier = getRandomExpectsCourier();
		randomCourier->setState(std::make_shared<DeliverCourierState>(randomCourier, nullptr));
	}
}

void CourierManager::release()
{
	for (auto &courier : couriers)
	{
		delete courier;
		courier = nullptr;
	}
	couriers.clear();
}

std::vector<Courier*> CourierManager::getCouriers() const
{
	return couriers;
}

Courier *CourierManager::getRandomExpectsCourier() {
	Courier *randomCourier = nullptr;
	int randomIndex;
	bool isExpectsCourier;
	do
	{
		randomIndex = rand() % Constants::CourierMaxCount;
		randomCourier = couriers[randomIndex];
		isExpectsCourier = typeid(*(randomCourier->getState().get())) == typeid(class ExpectsCourierState);
	} while (!isExpectsCourier);

	return randomCourier;
}