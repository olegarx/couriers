#pragma once
class Constants
{
public:
	static int WindowWidth;
	static int WindowHeight;
	static int CourierMaxCount;
	static int ActiveCourierCount;
	static double CourierProcessingTime;
	static double CourierSpeedMin;
	static double CourierSpeedMax;
	static double Radius;
	static double DeltaTime;

	static const double PI;
};

