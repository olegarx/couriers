#include "DeliverCourierState.h"
#include "TransfersCourierState.h"
#include "ReceivesCourierState.h"
#include "ExpectsCourierState.h"
#include "Constants.h"

#include <memory>

DeliverCourierState::DeliverCourierState(Courier *courier, Courier *oldCustomer) {
	this->courier = courier;
	this->oldCustomer = oldCustomer;
	this->targetCourier = nullptr;
}

void DeliverCourierState::delive(std::vector<Courier *> &couriers, double dt) {
	
	if (targetCourier == nullptr || typeid(*(targetCourier->getState().get())) != typeid(class ExpectsCourierState)) {
		targetCourier = selectRandomExpectsCourier(couriers);
	}

	if (targetCourier == nullptr) {
		return;
	}

	double targetX = targetCourier->getX();
	double targetY = targetCourier->getY();

	double vx = targetX - courier->getX();
	double vy = targetY - courier->getY();;

	double distance = std::sqrt(vx*vx + vy*vy);
	
	vx /= distance;
	vy /= distance;

	courier->setX(courier->getX() + vx * courier->getSpeed() * dt);
	courier->setY(courier->getY() + vy * courier->getSpeed() * dt);

	if (isAchivment()) {
		targetCourier->setState(std::make_shared<ReceivesCourierState>(targetCourier, courier));
		courier->setState(std::make_shared<TransfersCourierState>(courier));		
	} 
}
void DeliverCourierState::except() {

}
void DeliverCourierState::transfer() {
}
void DeliverCourierState::receive() {
}

bool DeliverCourierState::isAchivment() const {
	return (targetCourier != nullptr) && (abs(courier->getX() - targetCourier->getX()) < 0.1) && (abs(courier->getY() - targetCourier->getY()) < 0.1);
}

Courier *DeliverCourierState::selectRandomExpectsCourier(std::vector<Courier *> &couriers) {
	Courier *randomCourier = nullptr;
	int randomIndex;
	int attempts = 0;
	bool isExpectsCourier;
	do
	{
		randomIndex = rand() % Constants::CourierMaxCount;
		randomCourier = couriers[randomIndex];
		isExpectsCourier = typeid(*(randomCourier->getState().get())) == typeid(class ExpectsCourierState);
		attempts++;
	} while (((courier == randomCourier) || (randomCourier == oldCustomer) || !isExpectsCourier) && (attempts < 400));

	if (attempts >= 400) {
		randomCourier = nullptr;
		for (auto &courierItem : couriers) {
			if (typeid(*(courierItem->getState().get())) == typeid(class ExpectsCourierState) && (courierItem != oldCustomer) && (courierItem != courier)) {
				return courierItem;
			}
		}
	}

	return randomCourier;
}