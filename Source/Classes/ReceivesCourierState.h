#pragma once
#include "CourierState.h"
#include "Courier.h"
#include "Timer.h"

class ReceivesCourierState : public CourierState {
public:
	ReceivesCourierState(Courier *courier, Courier *deliver);

	void delive(std::vector<Courier *> &couriers, double dt);
	void except();
	void transfer();
	void receive();

private:
	Courier *courier;
	Courier *deliver;
	Timer timer;
};