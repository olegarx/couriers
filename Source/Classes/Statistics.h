#pragma once

#include "Courier.h"
#include <iostream>

class Statistics {
public:
	Statistics();

	void start(std::ostream *outputStream = &std::cout);
	void writeCouriersData(std::vector<Courier *> couriers);
	void writeChangesData(std::vector<Courier *> couriers);

private:
	std::string getCourierState(Courier *courier) const;

	std::vector<Courier> prevCouriers;
	std::ostream *outputStream;
	double startTime;
};