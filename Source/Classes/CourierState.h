#pragma once

#include <vector>
#include "Courier.h"

class CourierState {
public:
	virtual void delive(std::vector<Courier *> &couriers, double dt) = 0;
	virtual void except() = 0;
	virtual void transfer() = 0;
	virtual void receive() = 0;
};