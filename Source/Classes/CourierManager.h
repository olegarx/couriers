#pragma once
#include "courier.h"
#include <vector>

class CourierManager
{
public:
	enum class PositionType {Random, Circle};
	CourierManager();
	~CourierManager();

	void initialize(PositionType positionType);
	void update(double dt);
	void release();

	std::vector<Courier *> getCouriers() const;
private:
	std::vector<Courier *> couriers;

	Courier *getRandomExpectsCourier();
};

