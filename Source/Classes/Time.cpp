#include "time.h"

double Time::currentTime = 0.0;

void Time::increase(double dt)
{
	currentTime += dt;
}

double Time::getCurrentTime()
{
	return currentTime;
}