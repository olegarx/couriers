#include "Config.h"
#include <sstream>
#include <regex>

Config::Config() {
}

void Config::initialize(std::istream &configStream) {
	std::string configString("");
	char symbol;

	while (configStream) {
		configStream.read(&symbol, 1);
		if (configStream) {
			configString += symbol;
		}
	}

	std::regex regular(R"(([\w]*)[ ]*=[ ]*([\d]*.[\d]*))");
	std::smatch match;

	while (std::regex_search(configString, match, regular)) {
		std::string name(match[1].first, match[1].second);
		std::string value(match[2].first, match[2].second);
		double valueNumber = atof(value.c_str());
		configTable.insert(std::pair<std::string, double>(name, valueNumber));
		
		configString.erase(match[0].first, match[0].second);
	}
}

int Config::getWindowWidth() const {
	return static_cast<int>(getValue(windowWidth));
}
int Config::getWindowHeight() const {
	return static_cast<int>(getValue(windowHeight));
}
int Config::getCourierMaxCount() const {
	return static_cast<int>(getValue(courierMaxCount));
}
int Config::getActiveCourierCount() const {
	return static_cast<int>(getValue(activeCourierCount));
}
double Config::getCourierProcessingTime() const {
	return getValue(courierProcessingTime);
}
double Config::getCourierSpeedMin() const {
	return getValue(courierSpeedMin);
}
double Config::getCourierSpeedMax() const {
	return getValue(courierSpeedMax);
}
double Config::getRadius() const {
	return getValue(radius);
}
double Config::getDeltaTime() const {
	return getValue(deltaTime);
}


double Config::getValue(std::string name) const {
	auto findResult = configTable.find(name);
	if (findResult == configTable.end()) {
		throw std::exception((std::string("Can't find ") + name + std::string("'s value")).c_str());
	}
	return findResult->second;
}