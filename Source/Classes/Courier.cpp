#include "Courier.h"
#include <cmath>
#include "constants.h"
#include "CourierState.h"
#include <sstream>


Courier::Courier()
{
	countDelivers = 0;
	countDeliversString = "0";
}


Courier::~Courier()
{
}

double Courier::getX() const
{
	return x;
}

double Courier::getY() const
{
	return y;
}

double Courier::getSpeed() const
{
	return speed;
}

std::shared_ptr<CourierState> Courier::getState() const {
	return state;
}


void Courier::setState(const std::shared_ptr<CourierState> state)
{
	this->state = state;
}

void Courier::setX(double x)
{
	this->x = x;
}

void Courier::setY(double y)
{
	this->y = y;
}

void Courier::setSpeed(double speed)
{
	this->speed = speed;
}

void Courier::update(std::vector<Courier *> &couriers, double dt)
{
	state->delive(couriers, dt);
	state->except();
	state->receive();
	state->transfer();
}

int Courier::getCountDelivers() const {
	return countDelivers;
}
std::string Courier::getCountDeliversString() const {
	return countDeliversString;
}
void Courier::incrimentCountDelivers() {
	countDelivers++;
	std::stringstream stringStream;
	stringStream << countDelivers;
	countDeliversString = stringStream.str();
}

bool Courier::operator==(const Courier &courier) const {
	return (this->getCountDelivers() == courier.getCountDelivers()) &&
		   //(this->getSpeed() == courier.getSpeed()) &&
		   (typeid(this->getState().get()) == typeid(courier.getState().get()));
}