#include "Statistics.h"
#include "time.h"


#include "DeliverCourierState.h"
#include "ExpectsCourierState.h"
#include "ReceivesCourierState.h"
#include "TransfersCourierState.h"

Statistics::Statistics() {
	outputStream = &std::cout;
}

void Statistics::start(std::ostream *outputStream) {
	this->outputStream = outputStream;
	startTime = Time::getCurrentTime();
}
void Statistics::writeCouriersData(std::vector<Courier *> couriers) {
	unsigned int i = 0;
	for (auto courier : couriers) {
		(*outputStream) << "Courier #" << i << ":\t(speed = " << courier->getSpeed() << ");\n";
		i++;
	}
}
void Statistics::writeChangesData(std::vector<Courier *> couriers) {
	bool changed = prevCouriers.size() == 0;
	for (int i = 0; i < couriers.size() && i < prevCouriers.size(); ++i) {
		if (!(*couriers[i] == prevCouriers[i])) {
			changed = true;
			break;
		}
	}

	if (changed) {
		prevCouriers.clear();

		(*outputStream) << "Time: " << Time::getCurrentTime() - startTime << "\n";
		unsigned int i = 0;
		for (auto courier : couriers) {
			(*outputStream) << "Courier #" << i << ":\t(delivers = " << courier->getCountDelivers() << "; state = " << getCourierState(courier) << ");\n";
			prevCouriers.push_back(*courier);
			i++;
		}
	}
}

std::string Statistics::getCourierState(Courier *courier) const {
	if (typeid(*(courier->getState().get())) == typeid(class DeliverCourierState)) {
		return std::string("Deliver");
	}
	else if (typeid(*(courier->getState().get())) == typeid(class ReceivesCourierState)) {
		return std::string("Receives");
	}
	else if (typeid(*(courier->getState().get())) == typeid(class TransfersCourierState)) {
		return std::string("Transfers");
	}
	else if (typeid(*(courier->getState().get())) == typeid(class ExpectsCourierState)) {
		return std::string("Expects");
	}

	return std::string("None");
}