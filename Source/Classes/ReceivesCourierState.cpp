#include "ReceivesCourierState.h"
#include "DeliverCourierState.h"
#include "Constants.h"

ReceivesCourierState::ReceivesCourierState(Courier *courier, Courier *deliver) {
	this->courier = courier;
	this->deliver = deliver;
}

void ReceivesCourierState::delive(std::vector<Courier *> &couriers, double dt) {
}
void ReceivesCourierState::except() {

}
void ReceivesCourierState::transfer() {
}
void ReceivesCourierState::receive() {
	if (!timer.isStarted()) {
		timer.start(Constants::CourierProcessingTime);
	}
	if (timer.isTime()) {
		courier->setState(std::make_shared<DeliverCourierState>(courier, deliver));
	}
}