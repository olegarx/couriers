#include "Timer.h"
#include "time.h"

Timer::Timer()
{
	beginTime = Time::getCurrentTime();
	timeCount = 0;
	started = false;
}


Timer::~Timer()
{
}

void Timer::start(double timeCount)
{
	beginTime = Time::getCurrentTime();
	this->timeCount = timeCount;
	started = true;
}

void Timer::stop()
{
	started = false;
}

bool Timer::isTime() const
{
	return (Time::getCurrentTime() - beginTime) >= timeCount;
}

bool Timer::isStarted() const
{
	return started;
}