#pragma once
#include "CourierState.h"
#include "Courier.h"

class DeliverCourierState : public CourierState {
public:
	DeliverCourierState(Courier *courier, Courier *oldCustomer);

	void delive(std::vector<Courier *> &couriers, double dt);
	void except();
	void transfer();
	void receive();

private:
	bool isAchivment() const;

	Courier *selectRandomExpectsCourier(std::vector<Courier *> &couriers);

	Courier *courier;
	Courier *targetCourier;
	Courier *oldCustomer;
};