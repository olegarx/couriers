#pragma once

class Timer
{
public:
	Timer();
	~Timer();

	void start(double timeCount);
	void stop();
	bool isTime() const;
	bool isStarted() const;
private:
	double timeCount;
	double beginTime;
	bool started;
};

