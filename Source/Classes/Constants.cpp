#include "Constants.h"

int Constants::WindowWidth = 700;
int Constants::WindowHeight = 700;
int Constants::CourierMaxCount = 30;
int Constants::ActiveCourierCount = 15;
double Constants::CourierProcessingTime = 2;

double Constants::CourierSpeedMin = 0;
double Constants::CourierSpeedMax = 1;
double Constants::Radius = 100;
double Constants::DeltaTime = 0.1;

const double Constants::PI = 3.14159265358979323846;
