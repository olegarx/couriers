#pragma once

class Time
{
public:
	static void increase(double dt);
	static double getCurrentTime();
private:
	static double currentTime;
};