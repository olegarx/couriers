#pragma once

#include <unordered_map>
#include <string>
#include <iostream>

class Config {
public:
	Config();

	void initialize(std::istream &configStream);

	int getWindowWidth() const;
	int getWindowHeight() const;
	int getCourierMaxCount() const;
	int getActiveCourierCount() const;
	double getCourierProcessingTime() const;
	double getCourierSpeedMin() const;
	double getCourierSpeedMax() const;
	double getRadius() const;
	double getDeltaTime() const;

protected:
	double getValue(std::string name) const;

private:
	std::unordered_map<std::string, double> configTable;

	const std::string windowWidth = "windowWidth";
	const std::string windowHeight = "windowHeight";
	const std::string courierMaxCount = "courierMaxCount";
	const std::string activeCourierCount = "activeCourierCount";
	const std::string courierProcessingTime = "courierProcessingTime";
	const std::string courierSpeedMin = "courierSpeedMin";
	const std::string courierSpeedMax = "courierSpeedMax";
	const std::string radius = "radius";
	const std::string deltaTime = "deltaTime";
};