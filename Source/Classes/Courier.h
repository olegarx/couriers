#pragma once

#include <memory>
#include <vector>
#include <string>

class CourierState;

class Courier
{
public:
	Courier();
	~Courier();
	double getX() const;
	double getY() const;
	double getSpeed() const;
	std::shared_ptr<CourierState> getState() const;

	void setX(double x);
	void setY(double y);
	void setSpeed(double speed);
	void setState(const std::shared_ptr<CourierState> state);	
	
	void update(std::vector<Courier *> &couriers, double dt);

	int getCountDelivers() const;
	std::string getCountDeliversString() const;
	void incrimentCountDelivers();

	bool operator==(const Courier &courier) const;

private:
	double x;
	double y;
	double speed;

	std::string countDeliversString;
	int countDelivers;

	std::shared_ptr<CourierState> state;
};

