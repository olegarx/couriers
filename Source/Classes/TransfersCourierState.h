#pragma once
#include "CourierState.h"
#include "Courier.h"
#include "Timer.h"

class TransfersCourierState : public CourierState {
public:
	TransfersCourierState(Courier *courier);

	void delive(std::vector<Courier *> &couriers, double dt);
	void except();
	void transfer();
	void receive();

private:
	Courier *courier;
	Timer timer;
};