#include "TransfersCourierState.h"
#include "ExpectsCourierState.h"
#include "Constants.h"

TransfersCourierState::TransfersCourierState(Courier *courier) {
	this->courier = courier;
}

void TransfersCourierState::delive(std::vector<Courier *> &couriers, double dt) {
}
void TransfersCourierState::except() {

}
void TransfersCourierState::transfer() {
	if (!timer.isStarted()) {
		timer.start(Constants::CourierProcessingTime);
	}
	if (timer.isTime()) {
		courier->incrimentCountDelivers();
		courier->setState(std::make_shared<ExpectsCourierState>(courier));
	}
}
void TransfersCourierState::receive() {
}