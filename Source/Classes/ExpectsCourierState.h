#pragma once
#include "CourierState.h"
#include "Courier.h"

class ExpectsCourierState : public CourierState {
public:
	ExpectsCourierState(Courier *courier);

	void delive(std::vector<Courier *> &couriers, double dt);
	void except();
	void transfer();
	void receive();

private:
	Courier *courier;
};