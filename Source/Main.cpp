#include <windows.h>
#include <vector>
#include <cmath>
#include "../ConsoleApplication2/glut.h"
#include "Classes/constants.h"
#include "Classes/couriermanager.h"
#include <ctime>

#include "Classes\DeliverCourierState.h"
#include "Classes\ExpectsCourierState.h"
#include "Classes\ReceivesCourierState.h"
#include "Classes\TransfersCourierState.h"

#include "Classes\Config.h"

#include "Classes/time.h"

#include "Classes\Statistics.h"

#include <fstream>

#pragma comment(lib,"glut32.lib")

CourierManager courierManager;
Config config;
std::ofstream statisticsFile("statistics.txt");
Statistics statistics;

void drawCircle(double x, double y, double radius)
{
	double nextAngle;
	double step = Constants::PI / 32;
	for (double angle = 0; angle <= 2 * Constants::PI; angle += step)
	{
		nextAngle = angle + step;
		glVertex2f(x + radius * cos(angle), y + radius * sin(angle));
		glVertex2f(x + radius * cos(nextAngle), y + radius * sin(nextAngle));
	}
}

void renderBitmapString(float x, float y, void *font, const char *string) {
	const char *c;
	glRasterPos2f(x, y);
	for (c = string; *c != '\0'; c++) {
		glutBitmapCharacter(font, *c);
	}
}

double timeEllepsed = 0;
void Display() {
	auto beginTime = std::time(nullptr);
	courierManager.update(Constants::DeltaTime);
	statistics.writeChangesData(courierManager.getCouriers());

	std::vector<Courier *> couriers = courierManager.getCouriers();

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_LINES);

	int i = 0;
	for (auto courier : couriers)
	{
		glColor3f(1, 1, 1);
		drawCircle(courier->getX(), courier->getY(), 5);
	}

	
	glEnd();
	

	for (auto courier : couriers)
	{
		glColor3f(1, 1, 1);
		
		if (typeid(*(courier->getState().get())) == typeid(class DeliverCourierState) || typeid(*(courier->getState().get())) == typeid(class TransfersCourierState)) {
			renderBitmapString(courier->getX() - 15, courier->getY() - 5, GLUT_BITMAP_HELVETICA_12, courier->getCountDeliversString().c_str());
		}
		else {
			renderBitmapString(courier->getX() + 7, courier->getY() - 5, GLUT_BITMAP_HELVETICA_12, courier->getCountDeliversString().c_str());
		}

		
	}

	glutPostRedisplay();
	glFlush();

	Time::increase(Constants::DeltaTime);
	
	timeEllepsed = difftime(time(nullptr), beginTime);
}

void Initialize() {
	std::ifstream configFile("config.txt");
	config.initialize(configFile);

	Constants::WindowWidth = config.getWindowWidth();
	Constants::WindowHeight = config.getWindowHeight();
	Constants::ActiveCourierCount = config.getActiveCourierCount();
	Constants::CourierMaxCount = config.getCourierMaxCount();
	Constants::CourierSpeedMin = config.getCourierSpeedMin();
	Constants::CourierSpeedMax = config.getCourierSpeedMax();
	Constants::Radius = config.getRadius();
	Constants::CourierProcessingTime = config.getCourierProcessingTime();
	Constants::DeltaTime = config.getDeltaTime();

	configFile.close();

	glClearColor(0.8, 1.0, 0.6, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-Constants::WindowWidth / 2.0, Constants::WindowWidth / 2.0, -Constants::WindowHeight / 2.0, Constants::WindowHeight / 2.0, -5.0, 5.0);
	courierManager.initialize(CourierManager::PositionType::Circle);

	statistics.start(&statisticsFile);
	statistics.writeCouriersData(courierManager.getCouriers());
}

int main(int argc, char ** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(Constants::WindowWidth, Constants::WindowHeight);
	glutInitWindowPosition(100, 200);
	glutCreateWindow("Couriers");
	glutDisplayFunc(Display);
	Initialize();
	glutMainLoop();

	statisticsFile.close();

	return 0;
}